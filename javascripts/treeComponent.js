var buildTree = function(childs, container, parent) {
  childs.forEach(function(node){
    var newContainer = document.createElement('div');
    var newCheckbox = document.createElement('input');
    newCheckbox.type = 'checkbox';
    newCheckbox.name = node.name;
    newCheckbox.className += 'treeCbx';
    newCheckbox.setAttribute('data-parent', node.parent);
    var newLabel = document.createElement('label');
    newLabel.innerHTML = node.name;

    newContainer.appendChild(newCheckbox);
    newContainer.appendChild(newLabel);

    container.appendChild(newContainer);
    if(node.children) buildTree(node.children, newContainer, node);
  });
}

buildTree(root.children, document.getElementsByTagName('body')[0], root);

Array.from(document.getElementsByClassName('treeCbx')).forEach(function(cbx){
  cbx.addEventListener('change', function(){
    if (cbx.checked){
      Array.from(document.querySelectorAll('[data-parent="' + cbx.name + '"]')).forEach(function(child){
        child.checked = true;
      });
    } else {
        Array.from(document.querySelectorAll('[data-parent="' + cbx.name + '"]')).forEach(function(child){
          child.checked = false;
        });
        Array.from(document.querySelectorAll('[name="' + cbx.dataset.parent + '"]')).forEach(function(parent){
          parent.checked = false;
        });
    }
  });
});
