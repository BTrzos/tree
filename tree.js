var root = {
"name": "root",
"parent": null,
"children": [
      {
        "name": "name_a",
        "parent": "root",
        "children": [
          {
            "name": "name_a1",
            "parent": "name_a"
          },
          {
            "name": "name_a2",
            "parent": "name_a",
            "children": [
              {
                "name": "name_a21",
                "parent": "name_a2"
              },
              {
                "name": "name_a22",
                "parent": "name_a2"
              }
            ]
          }
        ]
      },
      {
        "name": "name_b",
        "parent": "root",
        "children": [
          {
            "name": "name_b1",
            "parent": "name_b",
            "children": [
              {
                "name": "name_b11",
                "parent": "name_b1"
              },
              {
                "name": "name_b12",
                "parent": "name_b1",
                "children": [
                  {
                    "name": "name_b121",
                    "parent": "name_b12"
                  }
                ]
              }
            ]
          },
          {
            "name": "name_b2",
            "parent": "name_b"
          }
        ]
      }
      ]
};
